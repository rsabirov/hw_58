import React, { Component } from 'react';
import './App.css';
import Modal from '../components/UI/Modal/Modal';
import Backdrop from '../components/UI/Backdrop/backdrop';
import Alert from '../components/UI/Alert/Alert';

class App extends Component {

  state = {
      modalShow: false,
      dismissed: false
  };

  modalShow = () => {

    this.setState({modalShow: true});
  };

  cancelHandler = () => {

      this.setState({modalShow: false});
  };

  dismissHandler = () => {
      this.setState({dismissed: true});
  };


  render() {

      return (
        <div className="App">
          <Backdrop show={this.state.modalShow}/>
          <div className='container'>
            <button type="button" className="btn btn-primary" onClick={() => {this.modalShow()}}>
              Launch demo modal
            </button>
            <Modal show={this.state.modalShow} closed={() => this.cancelHandler()} title="Some modal title">
              <div className="modal-body">
                Some content
              </div>
            </ Modal>
            <Alert type="warning" dismiss={() => this.dismissHandler()} status={this.state.dismissed}/>
            <Alert type="primary"/>
            <Alert type="success"/>
            <Alert type="danger" dismiss={() => this.dismissHandler()} status={this.state.dismissed}/>
          </div>
        </div>
    );
  }
}

export default App;
