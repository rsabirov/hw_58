import React from 'react';
import './modal.css';

const Modal = props => (
    <div className="Modal"   style={{
        transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
        opacity: props.show ? '1' : '0'
    }}
    >
        <div className="modal-content">
            <div className="modal-header">
                <h5 className="modal-title">{props.title}</h5>
                <button type="button" className="close" onClick={props.closed}>
                    <span>&times;</span>
                </button>
            </div>
            {props.children}
            <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
);

export default Modal;