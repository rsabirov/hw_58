import React from 'react';

const Alert = props => {

    const status = props.status ? 'none' : 'block';

    const classes = `alert alert-${props.type}`;

    return (
        <div className={classes} style={{display: `${status}`}}>
            <strong>Holy guacamole!</strong> You should check in on some of those fields below.
            {props.dismiss !== undefined && <button type="button" className="close" onClick={props.dismiss}>
                <span>&times;</span>
            </button>}
        </div>
    )
};

export default Alert;